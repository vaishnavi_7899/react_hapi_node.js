USE SampleDB
GO

CREATE PROCEDURE getBook @Name varchar(30)
AS
BEGIN
SELECT * FROM Books
WHERE Books.Person = @Name
END
GO
