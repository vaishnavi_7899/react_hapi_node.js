import redis from 'ioredis'
import { ResponseToolkit } from '@hapi/hapi';
import { config } from '../configurations/config';
const { v4: uuidv4 } = require('uuid');

class redisService 
{
    private redisClientInstance : any;
    constructor()
    {
        this.redisClientInstance = new redis.Cluster(config.get('redis').server.split(','),{ redisOptions : { password : config.get('redis').password } });
        
    }

    public createUUID = async(details: Object)=>
    {
        const id = uuidv4();
        await this.redisClientInstance.set(id,JSON.stringify(details),'EX',600);
        return id;
    }

    public getUUID = async(id:String) => {
        const user = await this.redisClientInstance.get(id);
        return JSON.parse(user);
    }

    public getUser = async(request:any,h: ResponseToolkit)=>
    {
        const user = await this.redisClientInstance.get(request.payload.id);
        return JSON.parse(user);   
    }

    public addKey = (request:any,h: ResponseToolkit)=>
    {
        return this.redisClientInstance.set(request.payload.key,request.payload.value);       
    }

}
export default new redisService();

