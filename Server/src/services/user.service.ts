import { ResponseToolkit } from '@hapi/hapi';
import sqlService from './sql.service';
import redisService from './redis.service';
const bcrypt = require('bcrypt');

class userService
{
    public home()
    {
        return "Welcome, to typescript Redis-Hapi-Node.js ";
    }

    public encryptPassword = async(password : String)=>
    {
        return await bcrypt.hash(password,10).then(async(hash:String) =>
        {
            return hash;
        });
    }

    public decryptPassword = async(password : String,encyptedPassword: String)=>
    {
        return await bcrypt.compare(password, encyptedPassword);            
    }

    public userDetailValidation= async(userDetails : any,password : String)=>
    {
        if(userDetails)
        {
            const checkPassword = await this.decryptPassword(password,userDetails.Password)
            return checkPassword;
        }
        else
        {
            return false;
        }
        
    }

    public registerUser = async(request: any,h:ResponseToolkit) => 
    {
        return await sqlService.getUserDetails(request.payload.id).then(async(Details:any)=>
        {
            if(!Details)
            {
                const hashPassword = await this.encryptPassword(request.payload.password);
                const payload = {
                    Id : request.payload.id,
                    Name : request.payload.name,
                    Email : request.payload.email,
                    Password : hashPassword
                }
                sqlService.addUser(payload);
                const uuid = await redisService.createUUID(payload);
                return  h.response({ message: "User Registration Success, UUID is generated",uuid,payload}).code(200);
            }
            else { return h.response({ message: "User Exists Already!!" }).code(400); }
        });
    }

    public loginUser = async(request: any,h:ResponseToolkit) =>
    {
        return await sqlService.getUserDetails(request.payload.id).then(async(Details:any)=>
        {
            const user = await this.userDetailValidation(Details,request.payload.password);
            if(user)
            {
                const uuid = await redisService.createUUID(Details);
                return  h.response({ message: "User Login Success, UUID is generated",uuid,Details}).code(200);   
            }
            else
            {
                return h.response({message: "Invalid Id or Invalid Password.."}).code(400);
            }
        }); 
    
    }
}

export default new userService();

