import { ResponseToolkit } from '@hapi/hapi';
import  Sql from '../database'; 

class sqlService
{
    private pool:any;
    constructor(){
        (async ()=>{
            this.pool = Sql.Connection();
        })();
    }

    bookDetails = async(request:any,h: ResponseToolkit) => {
        const book = await this.pool.then(async(sql : any)=>
        {
           return sql.request().query("exec getBook @Name='" + request.payload.person  + "';")
        });
        return book.recordset;       
    }

    getUserDetails= async(id : String)=>
    {
        return await this.pool.then(async(sql : any)=>
        {
            const user = await sql.request().query("exec getUser @Id ='" +  id + "';");
            return user.recordset[0];
        });
    }

    addUser = async(values : any)=>
    {
        const payload = {
            Id : values.Id,
            Name : values.Name,
            Email : values.Email,
            Password : values.Password
        }
        return await this.pool.then(async(sql : any)=>
        {
            return await sql.request().query("exec addUser @Id = '"+payload.Id +"',@Name = '"+payload.Name +"',@Email = '"+payload.Email +"',@Password = '"+payload.Password +"';");
        });
    }
  
}

export default new sqlService();

