import { ResponseToolkit, Server } from '@hapi/hapi';

const Boom = require('@hapi/boom');

const internals:any = {};

exports.plugin = {
  pkg : require('./package.json'),
  register(server:Server) {
    server.auth.scheme('jwt', internals.implementation);
  },
};

internals.implementation = (server:Server, options:any) => {
  const scheme = {
    authenticate: async (request:any, h:ResponseToolkit) => {
    
      const { authorization } = request.headers;
      
      const [tokenType, token] = authorization.split(/\s+/);

      if (!token || tokenType.toLowerCase() !== 'bearer') {
        throw Boom.unauthorized(null, tokenType, token);
      }

      const { isValid, credentials, artifacts } = await options.validate(request, token, h);
      
      if (!isValid) {
        return h.unauthenticated(Boom.unauthorized('Invalid UUID', tokenType), { credentials: credentials || {}, artifacts });
      }

      return h.authenticated({ credentials: credentials || {}, artifacts });
    },
  };
  return scheme;
};
