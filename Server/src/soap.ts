const soap = require('soap');
const path = require('path');
const WSDL_FILE = path.join(__dirname+"/soap/calculator.wsdl");
import { ResponseToolkit } from '@hapi/hapi';
import { config } from './configurations/config'

class Soap  {
    
    private soapClient : any;
    constructor(){
        this.soapClient = async ()=>
        {  
            return await soap.createClientAsync(WSDL_FILE,{ endpoint : config.get('soap').endpoint1});    
        }
    }
    
    public Addition = async(request:any,h:ResponseToolkit)=>{
        const payload = ({
            intA : request.payload.intA,
            intB : request.payload.intB
        })
        return await this.soapClient().then((client : any) => 
        {
            return client.AddAsync(payload).then((response:any) =>
            {
                return response[0].AddResult;
            })   
        })
        .catch((error:Error)=>{
            return error;
        })
    }

    public Subtraction = async(request:any,h:ResponseToolkit)=>{
        const payload = ({
            intA : request.payload.intA,
            intB : request.payload.intB
        })
        return await this.soapClient().then((client : any) => 
        {
            return client.SubtractAsync(payload).then((response:any) =>
            {
                return response[0].SubtractResult;
            })   
        })
        .catch((error:Error)=>{
            return error;
        })
    }

    public Multiplication = async(request:any,h:ResponseToolkit)=>{
        const payload = ({
            intA : request.payload.intA,
            intB : request.payload.intB
        })
        return await this.soapClient().then((client : any) => 
        {
            return client.MultiplyAsync(payload).then((response:any) =>
            {
                return response[0].MultiplyResult;
            })   
        })
        .catch((error:Error)=>{ 
            return error;
        })
    }

    public Division =async(request:any,h:ResponseToolkit)=>{
        const payload = ({
            intA : request.payload.intA,
            intB : request.payload.intB
        })
        return await this.soapClient().then((client : any) => 
        {
            return client.DivideAsync(payload).then((response:any) =>
            {
                return response[0].DivideResult;
            })   
        })
        .catch((error:Error)=>{
            return error;
        })
    }
  
}
export default new Soap();




