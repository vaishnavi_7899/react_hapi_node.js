import Joi from 'joi';

const authenticateUserSchema = Joi.object(
    {
        id: Joi.string().min(7).max(7).required(),
        password: Joi.string().min(6).max(255).required(),
    });

module.exports = authenticateUserSchema;
 
