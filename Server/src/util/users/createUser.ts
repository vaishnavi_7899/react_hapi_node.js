import Joi from 'joi';

const createUserSchema = Joi.object({
    id: Joi.string().min(7).max(7).required(),
    name: Joi.string().min(6).max(30).required(),
    email: Joi.string().min(6).max(30).required().email(),
    password: Joi.string().min(6).max(255).required(),
    });

module.exports = createUserSchema;
