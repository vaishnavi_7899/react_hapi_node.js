const convict = require('convict');
const path = require('path');

export const config = convict({

  host:
  {
    format: String,
    env: 'HOST',
  },

  port:
  {
    format: 'port',
    env: 'PORT',
  },

  database:
  {
    format: String,
    env: 'DATABASE',
  },

  redis: {
    format: String,
    env: 'REDIS',
  },

  soap: {
    format: String,
    env: 'SOAP',
  }

}).loadFile(path.join(__dirname, 'development.json'));

