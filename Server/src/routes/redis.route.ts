import { Server } from '@hapi/hapi';
import redisService  from '../services/redis.service';

export const redisRoute = (server: Server) => {
    server.route({
        method: 'POST',
        path: '/api/redis/view',
        options: 
        {
            auth : 'jwt',
            handler: redisService.getUser
        }
    });

    server.route({
        method: 'POST',
        path: '/api/redis/set-key',
        options: 
        {
            auth : 'jwt',
            handler: redisService.addKey
        }
    });

}
