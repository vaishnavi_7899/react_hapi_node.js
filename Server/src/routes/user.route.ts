import { Server } from '@hapi/hapi';
import userService from '../services/user.service';

const authenticateUserSchema = require('../util/users/loginUser');
const createUserSchema = require('../util/users/createUser');

export const userRoute = (server: Server) => {

    server.route({
        method: 'GET',
        path: '/',
        options : {
            auth : 'jwt',
            handler: userService.home
        }
    }); 

    server.route({
        method: 'POST',
        path: '/api/sql/register',
        handler: userService.registerUser,
        options: {
            validate: { payload : createUserSchema },
            auth : false
        }
    });
    server.route({
        method: 'POST',
        path: '/api/sql/login',
        handler: userService.loginUser,
        options: {
            validate: { payload : authenticateUserSchema },
            auth: false
        }
    });    
}

