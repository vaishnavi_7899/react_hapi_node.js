import { Server } from '@hapi/hapi';
import Soap from '../soap';

export const soapRoute = (server: Server) => {
    server.route({
        method: 'POST',
        path: '/api/soap/add',
        options: 
        {
            auth : 'jwt',
            handler: Soap.Addition
        }
    });

    server.route({
        method: 'POST',
        path: '/api/soap/subtract',
        options: 
        {
            auth : 'jwt',
            handler: Soap.Subtraction
        }
    });

    server.route({
        method: 'POST',
        path: '/api/soap/multiply',
        options: 
        {
            auth : 'jwt',
            handler: Soap.Multiplication
        }
    });

    server.route({
        method: 'POST',
        path: '/api/soap/divide',
        options: 
        {
            auth : 'jwt',
            handler: Soap.Division
        }
    });

}
