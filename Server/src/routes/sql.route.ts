import { Server } from '@hapi/hapi';
import sqlService from '../services/sql.service';

export const sqlRoute = (server: Server) => {
    server.route({
        method: 'POST',
        path: '/api/sql/book',
        options: 
        {
            auth : 'jwt',
            handler: sqlService.bookDetails
        }
    }); 
}
