import { ResponseToolkit, Server } from '@hapi/hapi';
import { userRoute } from './routes/user.route' ;
import { redisRoute } from './routes/redis.route';
import { sqlRoute } from './routes/sql.route' ;
import { soapRoute } from './routes/soap.route';
import { config } from './configurations/config';
import redisService from './services/redis.service';

const shell = require('shelljs');

const hapiAuthJWT = require('./lib/index');

export const init = async () => 
{ 
    const server: Server = new Server({port : config.get('port') ,host : config.get('host') });

    shell.cp('-R','src/static/*','dist/static'); 

    const validate = async(decoded:any, request:any, h:ResponseToolkit) => 
    {
        const [uuidType, uuid] = decoded.headers.authorization.split(/\s+/);
        
        const user = await redisService.getUUID(uuid);
        if(user)
        {   
            return { isValid: true }; 
        }
        else 
        {   
            return { isValid: false }; 
        }
    }

    await server.register(hapiAuthJWT);
    server.auth.strategy('jwt', 'jwt',{  validate });
    server.auth.default('jwt'); 

    userRoute(server);
    redisRoute(server);
    sqlRoute(server);
    soapRoute(server);
    
    try
    {
        server.start();
        console.log( 'Server running at: ' + server.info.uri);
        return server;
        
    } 
    catch (err) { console.error(err); }      
};
