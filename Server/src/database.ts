import * as sql from 'mssql';
import { config } from './configurations/config';

class Sql
{

   public Connection() 
   {
      return new sql.ConnectionPool(config.get('database')).connect()
      .then((pool : any) => {
         console.log('Connected to MSSQL');
         return pool;
      })
      .catch((err:string) => console.log('Database Connection Failed! Bad Config: ', err))   
   }
    
}
export default new Sql();
 
